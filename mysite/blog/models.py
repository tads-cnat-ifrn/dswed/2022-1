from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.forms.widgets import PasswordInput

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', 'email', 'first_name', 'last_name']
        widgets = {
            "password":PasswordInput(),
        }

class Postagem(models.Model):
    data = models.DateField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    titulo = models.CharField('Título', max_length=100)
    texto = models.TextField()
    def __str__(self):
        return self.titulo

class PostagemForm(ModelForm):
    class Meta:
        model = Postagem
        fields = ['titulo', 'texto']

class Comentario(models.Model):
    data = models.DateField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    texto = models.TextField(max_length=200)
    postagem = models.ForeignKey(Postagem, null=True, on_delete=models.CASCADE)
    def __str__(self):
        return self.texto

class ComentarioForm(ModelForm):
    class Meta:
        model = Comentario
        fields = ['texto']