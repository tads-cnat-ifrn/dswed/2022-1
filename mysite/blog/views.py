from django.utils import timezone
from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.views import View
from .models import Postagem, PostagemForm, ComentarioForm, UserForm
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db import connection

class NovoUsuarioView(View):
    def get(self, request, *args, **kwargs):
        form = UserForm()
        contexto = {'form': form}
        return render(request, 'blog/novo_usuario.html', contexto)

class IndexView(View):
    def get(self, request, *args, **kwargs):
        mes_atual = timezone.now().month
        posts = Postagem.objects.filter(data__month = mes_atual).order_by('-id')
        meses = self.get_meses()
        form = ComentarioForm()
        form2 = UserForm()
        contexto = {
            'posts': posts, 'mes': mes_atual, 'meses': meses,
            'form': form, 'form2': form2
        }
        return render(request, 'blog/index.html', contexto)
    def get_meses(self):
        with connection.cursor() as cursor:
            cursor.execute('select distinct data from blog_postagem')
            datas = cursor.fetchall()
        meses = {}
        mes_atual = timezone.now().month
        for d in datas:
            if d[0].month != mes_atual and not d[0].month in meses:
                meses[d[0].month] = d[0].month
        return meses.keys()

class ArquivoView(View):
    def get(self, request, *args, **kwargs):
        mes = kwargs['mes']
        posts = Postagem.objects.filter(data__month = mes).order_by('-id')
        contexto = {'posts': posts, 'mes': mes}
        return render(request, 'blog/arquivo.html', contexto)

@method_decorator(
    login_required(login_url='/blog/login'), name='dispatch'
)
class NovaPostagemView(View):
    def get(self, request, *args, **kwargs):
        form = PostagemForm()
        return render(request, 'blog/novo_post.html', {'form': form})
    def post(self, request, *args, **kwargs):
        form = PostagemForm(request.POST)
        if form.is_valid():
            post = form.save(commit = False)
            post.data = timezone.now()
            post.user = request.user
            post.save()
            messages.success(request, 'Postagem cadastrada com sucesso!')
            return HttpResponseRedirect(reverse('blog:index'))
        else:
            return render(request, 'blog/novo_post.html', {'form': form})

@method_decorator(
    login_required(login_url='/blog/login'), name='dispatch'
)
class NovoComentarioView(View):
    def post(self, request, *args, **kwargs):
        form = ComentarioForm(request.POST)
        if form.is_valid():
            coment = form.save(commit = False)
            coment.data = timezone.now()
            coment.user = request.user
            id_post = kwargs['postagem']
            p = Postagem.objects.get(pk=id_post)
            coment.postagem = p
            coment.save()
            messages.success(request, 'Comentário cadastrado com sucesso!')
        return HttpResponseRedirect(reverse('blog:index'))










