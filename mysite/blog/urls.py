from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

app_name = 'blog'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path(
        'usuario/novo/', views.NovoUsuarioView.as_view(),
        name='novo_usuario'
    ),
    path(
        'cadastro/',
        views.NovaPostagemView.as_view(), name='nova_postagem'
    ),
    path(
        '<int:postagem>/comenta/',
        views.NovoComentarioView.as_view(), name='novo_comentario'
    ),
    path(
        'arquivo/<int:mes>/',
        views.ArquivoView.as_view(), name='arquivo'
    ),
    path(
        'login/',
        auth_views.LoginView.as_view(template_name='blog/login.html'),
        name = 'login'
    )
]