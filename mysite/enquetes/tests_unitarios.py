import datetime
from django.utils import timezone
from django.test import TestCase
from .models import Pergunta

### TESTES UNITÁRIOS para a classe de modelo Pergunta
#####################################################
class PerguntaModeloTestes(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):
        '''
        DEVE retornar False para publicações com data no futuro.
        '''
        data_hora = timezone.now() + datetime.timedelta(seconds=1)
        obj_pergunta = Pergunta(data_pub = data_hora)
        self.assertIs(obj_pergunta.foi_publicada_recentemente(), False)

    def test_publicada_recentemente_com_pergunta_mais_antiga(self):
        '''
        DEVE retornar False para publicações mais antigas que 24hs.
        '''
        data_hora = timezone.now() - datetime.timedelta(
            hours = 24, seconds = 1
        )
        obj_pergunta = Pergunta(data_pub = data_hora)
        self.assertIs(obj_pergunta.foi_publicada_recentemente(), False)

    def test_publicada_recentemente_com_pergunta_recente(self):
        '''
        DEVE retornar True para publicações dentro das últimas 24hs.
        '''
        data_hora = timezone.now() - datetime.timedelta(
            hours = 23, minutes = 59, seconds = 59
        )
        obj_pergunta = Pergunta(data_pub = data_hora)
        self.assertIs(obj_pergunta.foi_publicada_recentemente(), True)