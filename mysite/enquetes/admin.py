from django.contrib import admin
from .models import Pergunta, Alternativa, Autor, Perfil, Rotulo

admin.site.site_header = 'Administração da Aplicação de Enquetes'

class AlternativaInline(admin.TabularInline):
    model = Alternativa
    extra = 2

class PerguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Informações base', {'fields': ['texto', 'imagem', 'autor']}),
        ('Informações de data', {'fields': ['data_pub', 'data_fim']}),
        ('Geral', {'fields': ['rotulos', 'possui_resposta']}),
    ]
    inlines = [AlternativaInline]
    list_display = [
        'texto', 'id', 'autor', 'data_pub', 'foi_publicada_recentemente'
    ]
    list_filter = ['data_pub', 'data_fim']
    search_fields = ['texto']

admin.site.register(Pergunta, PerguntaAdmin)
admin.site.register(Autor)
admin.site.register(Perfil)
admin.site.register(Rotulo)