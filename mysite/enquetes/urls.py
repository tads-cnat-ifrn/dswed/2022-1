from django.urls import path
from . import views
from django.views.generic import TemplateView

app_name = 'enquetes'
urlpatterns = [
    # ex.: /
    path('', views.IndexView.as_view(), name='index'),
    # ex.: enquetes/
    path('enquetes/', views.IndexViewJSON.as_view(), name='index_json'),
    # ex.: api/enquetes/
    path('api/enquetes/', views.GetEnquetesAPI.as_view(), name='get_enquetes'),
    # ex.: detalhes - /enquete/5/
    path(
        'enquete/<int:pk>/',
        views.DetalhesView.as_view(), name='detalhes'
    ),
    # ex.: resultado - /enquete/5/resultado/
    path(
        'enquete/<int:pk>/resultado/',
        views.ResultadoView.as_view(), name='resultado'
    ),
    # ex.: votação - /enquete/5/votacao/
    path(
        'enquete/<int:pk>/votacao/',
        views.VotacaoView.as_view(), name='votacao'
    ),
    path(
        'enquete/encerradas/',
        views.EncerradasView.as_view(), name='encerradas'
    ),
    path(
        'enquete/cadastrar/',
        views.NovaEnqueteView.as_view(), name='nova_enquete'
    ),
    path(
        'novo_layout/',
        TemplateView.as_view(
            template_name='enquetes/novo_layout.html'
        ), name='novo_layout'
    ),
    path(
        'login/',
        TemplateView.as_view(template_name="enquetes/login.html"), name='login'
    ),
]

