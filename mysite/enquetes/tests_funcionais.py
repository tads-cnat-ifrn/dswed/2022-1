import datetime
from django.utils import timezone
from django.urls import reverse
from django.test import TestCase
from .models import Pergunta

### Método de apoio - criação de perguntas no banco de dados
############################################################
def cria_pergunta(texto, quant_dias):
    '''
    Cria uma pergunta no banco com um dado texto e data de publicação
    '''
    data_hora = timezone.now() + datetime.timedelta(days = quant_dias)
    return Pergunta.objects.create(texto = texto, data_pub = data_hora)

### TESTES FUNCIONAIS para a view Index
#######################################
class IndexViewTests(TestCase):
    def test_sem_perguntas_cadastradas(self):
        '''
        Não havendo perguntas no banco, é exibida uma mensagem de erro
        '''
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma enquete cadastrada")
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'], []
        )

    def test_com_pergunta_no_passado(self):
        '''
        Perguntas com data de publicação no passado são listadas na index
        '''
        cria_pergunta(texto="pergunta no passado", quant_dias = -1)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "pergunta no passado")
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'],
            ['<Pergunta: 1 - pergunta no passado>']
        )

    def test_com_pergunta_no_futuro(self):
        '''
        Perguntas com data de publicação no futuro não devem ser listadas
        '''
        cria_pergunta(texto="pergunta no futuro", quant_dias = 1)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma enquete cadastrada")
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'], []
        )

    def test_com_pergunta_no_futuro_e_outra_no_passado(self):
        '''
        Devem ser exibidas apenas as perguntas no passado
        '''
        cria_pergunta(texto="pergunta no futuro", quant_dias = 1)
        cria_pergunta(texto="pergunta no passado", quant_dias = -1)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "pergunta no passado")
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'],
            ['<Pergunta: 2 - pergunta no passado>']
        )

    def test_com_duas_perguntas_no_passado(self):
        '''
        Devem exibir as perguntas em ordem (decrescente) pela data
        '''
        cria_pergunta(texto="pergunta no passado 1", quant_dias = -10)
        cria_pergunta(texto="pergunta no passado 2", quant_dias = -5)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'],
            ['<Pergunta: 2 - pergunta no passado 2>',
            '<Pergunta: 1 - pergunta no passado 1>']
        )

class DetalhesViewTests(TestCase):
    def test_com_pergunta_no_futuro(self):
        '''
        Deve ser retornado o erro 404, para perguntas no futuro.
        '''
        perg = cria_pergunta(texto="pergunta no futuro", quant_dias = 1)
        url = reverse('enquetes:detalhes', args=(perg.id,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 404)

    def test_com_pergunta_no_passado(self):
        '''
        Deve exibir normalmente perguntas com data no passado.
        '''
        perg = cria_pergunta(texto="pergunta no passado", quant_dias = -1)
        url = reverse('enquetes:detalhes', args=(perg.id,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, perg.texto)

    def test_com_identificador_inexistente(self):
        '''
        Deve retornar um erro (HTTP) 404, para identificador inválido
        '''
        cria_pergunta(texto="pergunta ok", quant_dias = -10)
        url = reverse('enquetes:detalhes', args=(99,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 404)

