# Generated by Django 3.2.3 on 2022-06-22 18:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('enquetes', '0004_auto_20220615_2038'),
    ]

    operations = [
        migrations.AddField(
            model_name='alternativa',
            name='correta',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='pergunta',
            name='possui_resposta',
            field=models.BooleanField(default=False),
        ),
    ]
