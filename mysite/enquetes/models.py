import datetime
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User

class Rotulo(models.Model):
    titulo = models.CharField('título', max_length = 30)
    class Meta:
        verbose_name = 'Rótulo'
    def __str__(self):
        return self.titulo


class Perfil(models.Model):
    descricao = models.TextField('descrição')
    naturalidade = models.CharField(max_length = 30)
    data_nasc = models.DateField('Data de nascimento')
    genero = models.CharField('Gênero literário', max_length = 30)
    def __str__(self):
        return self.descricao
    class Meta:
        verbose_name_plural = 'Perfis'


class Autor(models.Model):
    pseudonimo = models.CharField(max_length = 40)
    foto = models.ImageField(
        upload_to = 'autores', null = True, blank = True
    )
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
    perfil = models.OneToOneField(Perfil, on_delete=models.CASCADE)
    def __str__(self):
        return self.pseudonimo
    class Meta:
        verbose_name_plural = 'Autores'


class Pergunta(models.Model):
    texto = models.CharField(max_length = 100)
    imagem = models.ImageField(
        upload_to = 'perguntas', null = True, blank = True
    )
    data_pub = models.DateTimeField('Data de publicação')
    data_fim = models.DateField('Data de encerramento', null=True)
    autor = models.ForeignKey(
        Autor, on_delete = models.CASCADE, null=True
    )
    rotulos = models.ManyToManyField(Rotulo, blank=True)
    possui_resposta = models.BooleanField(default=False)
    #**** calcula o total de votos em uma enquete
    def total_votos(self):
        total = 0
        for alt in self.alternativa_set.all():
            total += alt.quant_votos
        return total
    #**** recupera as alternativas ordem decrescente de votos
    def get_resultado(self):
        return self.alternativa_set.orderby('-quant_votos')
    #**** retorna o estado da instância na forma de texto
    def __str__(self):
        return ("{0} - {1}").format(self.id, self.texto)
    #**** identifica se a pergunta foi publicada recentemente
    def foi_publicada_recentemente(self):
        agora = timezone.now()
        delta_24hs = datetime.timedelta(hours=24)
        return (
            (self.data_pub <= agora) and
            (self.data_pub >= agora - delta_24hs)
        )
    foi_publicada_recentemente.admin_order_field = 'data_pub'
    foi_publicada_recentemente.boolean = True
    foi_publicada_recentemente.short_description = 'Recente?'


class Alternativa(models.Model):
    texto = models.CharField(max_length = 50)
    quant_votos = models.IntegerField(
        'Quantidade de votos', default = 0
    )
    pergunta = models.ForeignKey(
        Pergunta, on_delete = models.CASCADE
    )
    correta = models.BooleanField(default=False)
    #**** retorna o estado da instância na forma de texto
    def __str__(self):
        return ("{0} - {1}").format(self.pergunta.texto, self.texto)
    #**** retorna a porcentagem de votos nessa alternativa específica
    def get_porcentagem(self):
        return (self.quant_votos / self.pergunta.total_votos()) * 100


class Voto(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    alternativa = models.ForeignKey(Alternativa, on_delete=models.CASCADE)







