import datetime
from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views import View
from .models import Pergunta, Alternativa, Rotulo

@method_decorator(login_required, name='dispatch')
class NovaEnqueteView(View):
    template = 'enquetes/nova_enquete.html'

    def get(self, request, *args, **kwargs):
        rotulos = Rotulo.objects.order_by('titulo')
        contexto = {'rotulos': rotulos}
        return render(request, self.template, contexto)
    def post(self, request, *args, **kwargs):
        ## Recurperar os dados enviados no formulário
        texto = request.POST['texto']
        encerramento = request.POST['data_fim']
        alt01 = request.POST['alt01']
        alt02 = request.POST['alt02']
        ## Testar se não são vazios, e criar e salvar os objetos
        if texto and encerramento and alt01 and alt02:
            if hasattr(request.user, 'autor'):
                publicacao = timezone.now()
                pergunta = Pergunta(
                    texto=texto, data_pub=publicacao, data_fim=encerramento,
                    autor = request.user.autor
                )
                pergunta.save()
                a1 = Alternativa(texto=alt01, pergunta=pergunta)
                a1.save()
                a2 = Alternativa(texto=alt02, pergunta=pergunta)
                a2.save()
                if request.POST['alt03']:
                    a3 = Alternativa(texto=request.POST['alt03'], pergunta=pergunta)
                    a3.save()
                if request.POST['alt04']:
                    a4 = Alternativa(texto=request.POST['alt04'], pergunta=pergunta)
                    a4.save()
                if request.POST['alt05']:
                    a5 = Alternativa(texto=request.POST['alt05'], pergunta=pergunta)
                    a5.save()
                if request.POST['alt06']:
                    a6 = Alternativa(texto=request.POST['alt06'], pergunta=pergunta)
                    a6.save()
                rotulos = request.POST.getlist('rotulos')
                for id_rotulo in rotulos:
                    r = Rotulo.objects.get(pk=id_rotulo)
                    pergunta.rotulos.add(r)
                    pergunta.save()
                ## Redirecionar para a view "index"
                messages.success(request, 'A enquete entitulada \"%s\" foi inserida com sucesso!'%pergunta.texto)
                return HttpResponseRedirect(reverse('enquetes:index'))
            else:
                messages.error(request, 'Não existe autor relacionado ao usuário logado!')
                return render(request, self.template)
        else:
            messages.error(request, 'Informe corretamente os parâmetros necessários!')
            return render(request, self.template)


#####
## Acessível a partir da URL /api/enquetes
#####
class GetEnquetesAPI(View):
    def get(self, request, *args, **kwargs):
        lista_enquetes = self.enquetes_ativas()
        return JsonResponse(list(lista_enquetes.values()), safe = False)
    #** recupera enquetes publicadas e com data de encerramento no futuro
    def enquetes_ativas(self):
        return Pergunta.objects.filter(
            data_pub__lte = timezone.now()
        ).filter(
            data_fim__gt = datetime.date.today()
        ).order_by('-data_pub')

#####
## Acessível a partir da URL /enquetes
#####
class IndexViewJSON(View):
    def get(self, request, *args, **kwargs):
        lista_enquetes = self.enquetes_ativas()
        contexto = { 'pergunta_list': lista_enquetes }
        return render(
            request, 'enquetes/enquetes.json', contexto,
            content_type = 'application/json'
        )
    #** recupera enquetes publicadas e com data de encerramento no futuro
    def enquetes_ativas(self):
        return Pergunta.objects.filter(
            data_pub__lte = timezone.now()
        ).filter(
            data_fim__gt = datetime.date.today()
        ).order_by('-data_pub')


class IndexView(View):
    def get(self, request, *args, **kwargs):
        lista_enquetes = self.enquetes_ativas()
        contexto = { 'pergunta_list': lista_enquetes }
        return render(request, 'enquetes/index.html', contexto)
    #** recupera enquetes publicadas e com data de encerramento no futuro
    def enquetes_ativas(self):
        return Pergunta.objects.filter(
            data_pub__lte = timezone.now()
        ).filter(
            data_fim__gt = datetime.date.today()
        ).order_by('-data_pub')
    #** recupera enquetes ativas que possuam um dado texto ("ignore case")
    def busca_enquete_ativa(self, trecho):
        return self.enquetes_ativas().filter(texto__icontains = trecho)


class EncerradasView(View):
    def get(self, request, *args, **kwargs):
        lista_enquetes = self.enquetes_encerradas()
        contexto = { 'pergunta_list': lista_enquetes }
        return render(request, 'enquetes/encerradas.html', contexto)
    #** recupera enquetes com data de encerramento no passado
    def enquetes_encerradas(self):
        return Pergunta.objects.filter(
            data_fim__lt = datetime.date.today()
        )

class DetalhesView(View):
    def get(self, request, *args, **kwargs):
        if 'msg' in request.session:
            del request.session['msg']
        id_pergunta = kwargs['pk']
        pergunta = get_object_or_404(Pergunta, pk = id_pergunta)
        if pergunta.data_pub > timezone.now():
            raise Http404("Identificador inválido informado.")
        contexto = { 'pergunta': pergunta }
        return render(request, 'enquetes/pergunta_detail.html', contexto)

class VotacaoView(View):
    def post(self, request, *args, **kwargs):
        id_pergunta = kwargs['pk']
        pergunta = get_object_or_404(Pergunta, pk = id_pergunta)
        try:
            id_opcao = request.POST['alternativa']
            selecionada = pergunta.alternativa_set.get(pk = id_opcao)
        except (KeyError, Alternativa.DoesNotExist):
            contexto = {
                'pergunta': pergunta,
                'msg_erro': 'Selecione uma opção válida!'
            }
            return render(request, 'enquetes/detalhes.html', contexto)
        else:
            selecionada.quant_votos += 1
            selecionada.save()
            return HttpResponseRedirect(
                reverse('enquetes:resultado', args=(pergunta.id,))
            )

class ResultadoView(View):
    def get(self, request, *args, **kwargs):
        id_pergunta = kwargs['pk']
        pergunta = get_object_or_404(Pergunta, pk = id_pergunta)
        contexto = { 'pergunta': pergunta }
        return render(request, 'enquetes/resultado.html', contexto)


'''
#######  Histórico da implementação dos elementos de View #######
#################################################################

##### Autorizando usuários de um grupo específico
### Função com o critério desejado
def eh_funcionario(user):
    for grp in user.groups.all():
        if grp.name == 'funcionario':
            return True
    return False
### Decoração a ser utilizada na classe de visão
@method_decorator(user_passes_test(eh_funcionario), name='dispatch')

########  VIEW INDEX ########
#############################

##### Versão 1 - função de view
def index(request):
    # recupera as informações a serem apresentadas
    lista_enquetes = Pergunta.objects.order_by('-data_pub')[:25]
    # cria o mapa de contexto a ser acessado pelo template
    contexto = { 'lista_enquetes': lista_enquetes }
    # solicita a renderização do template
    return render(request, 'enquetes/index.html', contexto)

##### Versão 2 - subclasse de classe genérica - ListView
class IndexView(generic.ListView):
    template_name = 'enquetes/index.html'
    def get_queryset(self):
        return Pergunta.objects.order_by('-data_pub')[:25]

######  VIEW DETALHES #######
#############################

##### Versão 1 - função de view
def detalhes(request, id_pergunta):
    # recupera as informações a serem apresentadas
    pergunta = get_object_or_404(Pergunta, pk = id_pergunta)
    # cria o mapa de contexto a ser acessado pelo template
    contexto = { 'pergunta': pergunta }
    # solicita a renderização do template
    return render(request, 'enquetes/detalhes.html', contexto)

##### Versão 2 - subclasse de classe genérica - DetailView
class DetalhesView(generic.DetailView):
    model = Pergunta

####### VIEW VOTAÇÃO ########
#############################

##### Versão 1 - função de view
def votacao(request, id_pergunta):
    pergunta = get_object_or_404(Pergunta, pk = id_pergunta)
    try:
        id_opcao = request.POST['alternativa']
        selecionada = pergunta.alternativa_set.get(pk = id_opcao)
    except (KeyError, Alternativa.DoesNotExist):
        contexto = {
            'pergunta': pergunta,
            'msg_erro': 'Selecione uma opção válida!'
        }
        return render(request, 'enquetes/detalhes.html', contexto)
    else:
        selecionada.quant_votos += 1
        selecionada.save()
        return HttpResponseRedirect(
            reverse('enquetes:resultado', args=(pergunta.id,))
        )

#####  VIEW RESULTADOS ######
#############################

##### Versão 1 - função de view
def resultado(request, id_pergunta):
    # recupera as informações a serem apresentadas
    pergunta = get_object_or_404(Pergunta, pk = id_pergunta)
    # cria o mapa de contexto a ser acessado pelo template
    contexto = { 'pergunta': pergunta }
    # solicita a renderização do template
    return render(request, 'enquetes/resultado.html', contexto)

##### Versão 2 - Subclasse de classe genérica - DetailView
class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/resultado.html'

'''